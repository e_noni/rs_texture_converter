## Niclas Schlapmann - Freelance 3D Generalist
## www.enoni.de
## hello@enoni.de
## ns_Version - RS Texture Converter 1.04
## 29.09.2019
##################################### Imports ####################################
import hou
##################################################################################
import os
import sys
import subprocess
from multiprocessing import Queue
from Queue import Queue
##################################################################################
from PySide2 import QtGui, QtWidgets, QtCore, QtUiTools
from PySide2.QtWidgets import QTableWidgetItem
from PySide2.QtGui import QPainter, QColor, QPen
from PySide2.QtGui import *
from PySide2.QtCore import *
from PySide2.QtWidgets import *
from PySide2.QtCore import *
from PySide2.QtUiTools import *
##################################################################################
from ns_Version import resource
##################################################################################
try:
    rsTexProcessorPath = "redshiftTextureProcessor"
    plugin_folder = hou.getenv("HOUDINI_USER_PREF_DIR")
    plugin_folder = plugin_folder + os.sep + "scripts" + os.sep + "python" + os.sep + "ns_Version"
except Exception as e:
    #print('Error on line {}'.format(sys.exc_info()[-1].tb_lineno), type(e).__name__, e)
    hou.ui.displayMessage("Check your Environment Variables.")
############################### Init Data/Icons ##################################
iconSelected = QtGui.QIcon(QtGui.QPixmap(plugin_folder + os.sep + "Icons/selectedIcon.png"))
iconUnselected = QtGui.QIcon(QtGui.QPixmap(plugin_folder + os.sep + "Icons/unselectedIcon.png"))
##################################################################################



class ns_Version_Convert_QThread(QThread):
    def __init__(self, queue, fileNames, parent=None):
        QThread.__init__(self)
        self.queue = queue
        self.fileNames = fileNames


    def __del__(self):
        self.wait()


    def run(self):
        self.emit(SIGNAL("ns_Version_Convert_QThread_button_state_QString(QString)"), "disable")
        self.emit(SIGNAL("ns_Version_Convert_QThread_push_text_QString(QString)"), "Start Convert.")
        i = 0
        while not self.queue.empty():
            value = self.queue.get()
            i += 1
            self.emit(SIGNAL("ns_Version_Convert_QThread_push_text_QString(QString)"), "Convert: " + self.fileNames[i-1])
            startupinfo = subprocess.STARTUPINFO()
            startupinfo.dwFlags = subprocess.STARTF_USESHOWWINDOW
            p = subprocess.Popen(rsTexProcessorPath.replace("/", "\\") + " " + value, stdout=subprocess.PIPE, startupinfo=startupinfo)
            out = p.communicate()[0]
            self.queue.task_done()
            perc = (100 * i) / (len(self.fileNames))
            self.emit(SIGNAL("ns_Version_Convert_QThread_push_button_text_QString(QString)"), ("Convert Textures: ({})").format(len(self.fileNames) - i))
            self.emit(SIGNAL("ns_Version_Convert_QThread_push_perc_QString(QString)"), str(perc))
        self.emit(SIGNAL("ns_Version_Convert_QThread_push_text_QString(QString)"), "Convert finished.")
        self.emit(SIGNAL("ns_Version_Convert_QThread_push_perc_QString(QString)"), str(0))
        self.emit(SIGNAL("ns_Version_Convert_QThread_push_update_QString(QString)"), "True")
        self.emit(SIGNAL("ns_Version_Convert_QThread_button_state_QString(QString)"), "enable")


class ns_Version_rs_texture_converter(QtWidgets.QWidget):
    convertMode = 2
    overwrite = True
    selected_row = 0
    selected_column = 0

    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        ui_file = QtCore.QFile(plugin_folder + "/UIs/ns_redshift_texture_converter.ui")
        ui_file.open(QtCore.QFile.ReadOnly)
        self.ns_Version_rs_texture_converter = QtUiTools.QUiLoader().load(ui_file, parentWidget=self)
        self.ns_Version_rs_texture_converter.tableWidget_relevantObjects.setItemDelegate(QtWidgets.QItemDelegate())

        ## for widget resizing ##
        layout_m = QtWidgets.QVBoxLayout()
        layout_m.setContentsMargins(0, 0, 0, 0)
        self.setLayout(layout_m)
        layout_m.addWidget(self.ns_Version_rs_texture_converter)

        ## SIGNALS ##
        ## Check-Boxes ##
        self.connect(self.ns_Version_rs_texture_converter.checkBox_auto, QtCore.SIGNAL('toggled(bool)'), self.setBooleanColors_auto)
        self.connect(self.ns_Version_rs_texture_converter.checkBox_lin, QtCore.SIGNAL('toggled(bool)'), self.setBooleanColors_lin)
        self.connect(self.ns_Version_rs_texture_converter.checkBox_srgb, QtCore.SIGNAL('toggled(bool)'), self.setBooleanColors_srgb)
        self.connect(self.ns_Version_rs_texture_converter.checkBox_custom, QtCore.SIGNAL('toggled(bool)'), self.setBooleanColors_custom)
        self.connect(self.ns_Version_rs_texture_converter.checkBox_overwrite, QtCore.SIGNAL('toggled(bool)'), self.setBooleanColors_overwrite)

        ## QTable-Widget ##
        self.connect(self.ns_Version_rs_texture_converter.tableWidget_relevantObjects, QtCore.SIGNAL("cellClicked(int, int)"), self.changeTableWidgetValue)

        ## Buttons ##
        self.connect(self.ns_Version_rs_texture_converter.pushButton_convert, QtCore.SIGNAL('clicked()'), self.convert)
        self.connect(self.ns_Version_rs_texture_converter.pushButton_select_all, QtCore.SIGNAL('clicked()'), self.select_all)
        self.connect(self.ns_Version_rs_texture_converter.pushButton_invert_select, QtCore.SIGNAL('clicked()'), self.select_invert)
        self.connect(self.ns_Version_rs_texture_converter.pushButton_open_location, QtCore.SIGNAL('clicked()'), self.open_location)
        self.connect(self.ns_Version_rs_texture_converter.pushButton_select_node, QtCore.SIGNAL('clicked()'), self.select_node)
        self.connect(self.ns_Version_rs_texture_converter.pushButton_update_list, QtCore.SIGNAL('clicked()'), self.update_list)
        ## SIGNALS - END ##

        ## GUI-Init ##
        self.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)
        self.setWindowTitle("ns_Version - RS Texture Converter")
        self.setParent(hou.qt.mainWindow(), QtCore.Qt.Window)
        self.show()


        ## Collect Data ##
        self.gatherObjects()


    def update_list(self):
        self.gatherObjects()


    def open_location(self):
        try:
            counterItem = self.ns_Version_rs_texture_converter.tableWidget_relevantObjects.item(self.selected_row, 5)
            entry = counterItem.toolTip().split("\n")
            pathParts = entry[0].split("/")
            pathParts.pop()
            ns_path = ""

            for i in pathParts:
                ns_path = ns_path + i + "/"
            if os.path.exists(ns_path.replace("/", os.sep)):
                if sys.platform == "darwin":  # macOS
                    subprocess.Popen(["open", "--", ns_path.replace("/", os.sep)])
                if sys.platform == "linux2":  # Linux
                    subprocess.Popen(["xdg-open", "--", ns_path.replace("/", os.sep)])
                if sys.platform == "win32":  # Windows
                    subprocess.Popen(["explorer", ns_path.replace("/", os.sep)])
            else:
                pass
                # hou.ui.displayMessage("Cache location dont exist!")
        except Exception as e:
            pass
            # print('Error on line {}'.format(sys.exc_info()[-1].tb_lineno), type(e).__name__, e)
            # hou.ui.displayMessage("Can`t open Location!")


    def select_node(self):
        try:
            objectItem = self.ns_Version_rs_texture_converter.tableWidget_relevantObjects.item(self.selected_row, 2)
            path = objectItem.toolTip()
            nodeToSelect = hou.node(path)
            nodeToSelect.setSelected(True)
        except Exception as e:
            pass
            # print('Error on line {}'.format(sys.exc_info()[-1].tb_lineno), type(e).__name__, e)
            # hou.ui.displayMessage("Can`t open Location!")


    def select_all(self):
        ## Select all chkBox items ##
        rowCount = self.ns_Version_rs_texture_converter.tableWidget_relevantObjects.rowCount()
        for i in xrange(rowCount):
            chkBoxItem = self.ns_Version_rs_texture_converter.tableWidget_relevantObjects.item(i, 6)
            chkBoxItem.setIcon(iconSelected)
            chkBoxItem.setToolTip("Include")


    def select_invert(self):
        ## Invert chkBox item selection ##
        rowCount = self.ns_Version_rs_texture_converter.tableWidget_relevantObjects.rowCount()
        for i in xrange(rowCount):
            chkBoxItem = self.ns_Version_rs_texture_converter.tableWidget_relevantObjects.item(i, 6)
            if chkBoxItem.toolTip() == "Include":
                chkBoxItem.setIcon(iconUnselected)
                chkBoxItem.setToolTip("Exclude")
            else:
                chkBoxItem.setIcon(iconSelected)
                chkBoxItem.setToolTip("Include")


    def set_button_text(self, text):
        self.ns_Version_rs_texture_converter.pushButton_convert.setText(text)
        if text == "Convert finished.":
            self.gatherObjects()



    def set_button_state(self, text):
        if text == "disable":
            self.ns_Version_rs_texture_converter.pushButton_convert.setEnable(False)
        elif text == "enable":
            self.ns_Version_rs_texture_converter.pushButton_convert.setEnable(True)


    def set_update(self, text):
        self.gatherObjects()


    def set_status_bar_text(self, text):
        self.ns_Version_rs_texture_converter.label_status.setText(text)


    def set_status_bar_perc(self, perc):
        self.ns_Version_rs_texture_converter.progressBar_status.setValue(int(perc))


    def convert(self):
        filesNames = []
        cmdList = []
        rowCount = self.ns_Version_rs_texture_converter.tableWidget_relevantObjects.rowCount()

        ## Gather to converting Files ##
        for i in xrange(rowCount):
            chkBoxItem = self.ns_Version_rs_texture_converter.tableWidget_relevantObjects.item(i, 6)
            if chkBoxItem.toolTip() == "Include":
                counterItem = self.ns_Version_rs_texture_converter.tableWidget_relevantObjects.item(i, 5)
                toolTip = counterItem.toolTip()
                files = toolTip.split("\n")
                for ii in files:
                    if ii != "":
                        mode = self.ns_Version_rs_texture_converter.lineEdit_custom_input.text()
                        cmdList.append(str(ii).replace(" ", "_") + mode)

                        fileNamesParts = ii.split("/")
                        filesNames.append(fileNamesParts[-1])
        queue = Queue()
        for i in cmdList:
            queue.put(i)

        self.convertThread = ns_Version_Convert_QThread(queue, filesNames)
        self.connect(self.convertThread, SIGNAL("ns_Version_Convert_QThread_push_text_QString(QString)"), self.set_status_bar_text)
        self.connect(self.convertThread, SIGNAL("ns_Version_Convert_QThread_push_perc_QString(QString)"), self.set_status_bar_perc)
        self.connect(self.convertThread, SIGNAL("ns_Version_Convert_QThread_push_update_QString(QString)"), self.set_update)
        self.connect(self.convertThread, SIGNAL("ns_Version_Convert_QThread_push_button_text_QString(QString)"), self.set_button_text)
        self.connect(self.convertThread, SIGNAL("ns_Version_Convert_QThread_push_button_state_QString(QString)"), self.set_button_state)
        self.convertThread.start()

        barVer = self.ns_Version_rs_texture_converter.tableWidget_relevantObjects.verticalScrollBar()
        barHor = self.ns_Version_rs_texture_converter.tableWidget_relevantObjects.horizontalScrollBar()
        self.ns_Version_rs_texture_converter.tableWidget_relevantObjects.scrollContentsBy(barHor.value(), barVer.value())


    def changeTableWidgetValue(self, row, col):
        self.selected_column = col
        self.selected_row = row

        ## Disconnect Signal ##
        self.disconnect(self.ns_Version_rs_texture_converter.tableWidget_relevantObjects, QtCore.SIGNAL("cellChanged(int, int)"), self.changeTableWidgetValue)

        ## Change State chkBox Item ##
        if col == 6:
            chkBoxItem = self.ns_Version_rs_texture_converter.tableWidget_relevantObjects.item(row, col)
            if chkBoxItem.toolTip() == "Include":
                chkBoxItem.setIcon(iconUnselected)
                chkBoxItem.setToolTip("Exclude")
            else:
                chkBoxItem.setIcon(iconSelected)
                chkBoxItem.setToolTip("Include")

        ## Calc overall picture count to convert ##
        rowCount = self.ns_Version_rs_texture_converter.tableWidget_relevantObjects.rowCount()
        overallCounter = 0
        for i in xrange(rowCount):
            chkBoxItem = self.ns_Version_rs_texture_converter.tableWidget_relevantObjects.item(i, 6)
            counterItem = self.ns_Version_rs_texture_converter.tableWidget_relevantObjects.item(i, 5)
            if chkBoxItem.toolTip() == "Include":
                overallCounter = overallCounter + int(counterItem.text())
        self.ns_Version_rs_texture_converter.pushButton_convert.setText("Convert Textures (" + str(overallCounter) + ")")

        ## Readding Signal ##
        self.connect(self.ns_Version_rs_texture_converter.tableWidget_relevantObjects, QtCore.SIGNAL("cellChanged(int, int)"), self.changeTableWidgetValue)


    def setBooleanColors_overwrite(self, checkState):
        if checkState:
            self.ns_Version_rs_texture_converter.checkBox_overwrite.setStyleSheet('''
                QCheckBox{
                color: rgb(255, 255, 255); 
                background-color: rgb(0, 140, 0);
                border: 1px solid rgb(0, 255, 0);
                }''')
            self.overwrite = True
            text = self.ns_Version_rs_texture_converter.lineEdit_custom_input.text()
            text = text + " -noskip"
            self.ns_Version_rs_texture_converter.lineEdit_custom_input.setText(text)
        else:
            self.ns_Version_rs_texture_converter.checkBox_overwrite.setStyleSheet('''
                QCheckBox{
                color: rgb(255, 170, 0);
                background-color: rgb(0, 0, 0);
                border: 1px solid rgb(0, 0, 0);
                }''')
            self.overwrite = False
            text = self.ns_Version_rs_texture_converter.lineEdit_custom_input.text()
            parts = text.split(" -noskip")
            if text == " -noskip":
                self.ns_Version_rs_texture_converter.lineEdit_custom_input.setText("")
            else:
                self.ns_Version_rs_texture_converter.lineEdit_custom_input.setText(parts[0])


    def setBooleanColors_lin(self, checkState):
        if checkState:
            self.ns_Version_rs_texture_converter.checkBox_srgb.setChecked(False)
            self.ns_Version_rs_texture_converter.checkBox_srgb.setStyleSheet('''
                QCheckBox{
                color: rgb(255, 170, 0); 
                background-color: rgb(0, 0, 0);
                border: 1px solid rgb(0, 0, 0);
                }''')
            self.ns_Version_rs_texture_converter.checkBox_auto.setChecked(False)
            self.ns_Version_rs_texture_converter.checkBox_auto.setStyleSheet('''
                QCheckBox{
                color: rgb(255, 170, 0); 
                background-color: rgb(0, 0, 0);
                border: 1px solid rgb(0, 0, 0);
                }''')
            self.ns_Version_rs_texture_converter.checkBox_custom.setChecked(False)
            self.ns_Version_rs_texture_converter.checkBox_custom.setStyleSheet('''
                QCheckBox{
                color: rgb(255, 170, 0);
                background-color: rgb(0, 0, 0);
                border: 1px solid rgb(0, 0, 0);
                }''')
            self.ns_Version_rs_texture_converter.lineEdit_custom_input.setEnabled(False)
            self.ns_Version_rs_texture_converter.checkBox_lin.setStyleSheet('''
                QCheckBox{
                color: rgb(255, 255, 255); 
                background-color: rgb(0, 140, 0);
                border: 1px solid rgb(0, 255, 0);
                }''')
            if self.ns_Version_rs_texture_converter.checkBox_overwrite.isChecked():
                self.ns_Version_rs_texture_converter.lineEdit_custom_input.setText(" -l -noskip")
            else:
                self.ns_Version_rs_texture_converter.lineEdit_custom_input.setText(" -l")
            self.convertMode = 0
        else:
            self.ns_Version_rs_texture_converter.checkBox_lin.setStyleSheet('''
                QCheckBox{
                color: rgb(255, 170, 0);
                background-color: rgb(0, 0, 0);
                border: 1px solid rgb(0, 0, 0);
                }''')


    def setBooleanColors_srgb(self, checkState):
        if checkState:
            self.ns_Version_rs_texture_converter.checkBox_lin.setChecked(False)
            self.ns_Version_rs_texture_converter.checkBox_lin.setStyleSheet('''
                QCheckBox{
                color: rgb(255, 170, 0); 
                background-color: rgb(0, 0, 0);
                border: 1px solid rgb(0, 0, 0);
                }''')
            self.ns_Version_rs_texture_converter.checkBox_auto.setChecked(False)
            self.ns_Version_rs_texture_converter.checkBox_auto.setStyleSheet('''
                QCheckBox{
                color: rgb(255, 170, 0);
                background-color: rgb(0, 0, 0);
                border: 1px solid rgb(0, 0, 0);
                }''')
            self.ns_Version_rs_texture_converter.checkBox_custom.setChecked(False)
            self.ns_Version_rs_texture_converter.checkBox_custom.setStyleSheet('''
                QCheckBox{
                color: rgb(255, 170, 0);
                background-color: rgb(0, 0, 0);
                border: 1px solid rgb(0, 0, 0);
                }''')
            self.ns_Version_rs_texture_converter.lineEdit_custom_input.setEnabled(False)
            self.ns_Version_rs_texture_converter.checkBox_srgb.setStyleSheet('''
                QCheckBox{
                color: rgb(255, 255, 255);
                background-color: rgb(0, 140, 0);
                border: 1px solid rgb(0, 255, 0);
                }''')
            if self.ns_Version_rs_texture_converter.checkBox_overwrite.isChecked():
                self.ns_Version_rs_texture_converter.lineEdit_custom_input.setText(" -s -noskip")
            else:
                self.ns_Version_rs_texture_converter.lineEdit_custom_input.setText(" -s")
            self.convertMode = 1
        else:
            self.ns_Version_rs_texture_converter.checkBox_srgb.setStyleSheet('''
                QCheckBox{
                color: rgb(255, 170, 0);
                background-color: rgb(0, 0, 0);
                border: 1px solid rgb(0, 0, 0);
                }''')


    def setBooleanColors_auto(self, checkState):
        if checkState:
            self.ns_Version_rs_texture_converter.checkBox_lin.setChecked(False)
            self.ns_Version_rs_texture_converter.checkBox_lin.setStyleSheet('''
                QCheckBox{
                color: rgb(255, 170, 0);
                background-color: rgb(0, 0, 0);
                border: 1px solid rgb(0, 0, 0);
                }''')
            self.ns_Version_rs_texture_converter.checkBox_srgb.setChecked(False)
            self.ns_Version_rs_texture_converter.checkBox_srgb.setStyleSheet('''
                QCheckBox{
                color: rgb(255, 170, 0);
                background-color: rgb(0, 0, 0);
                border: 1px solid rgb(0, 0, 0);
                }''')
            self.ns_Version_rs_texture_converter.checkBox_custom.setChecked(False)
            self.ns_Version_rs_texture_converter.checkBox_custom.setStyleSheet('''
                QCheckBox{
                color: rgb(255, 170, 0);
                background-color: rgb(0, 0, 0);
                border: 1px solid rgb(0, 0, 0);
                }''')
            self.ns_Version_rs_texture_converter.lineEdit_custom_input.setEnabled(False)
            self.ns_Version_rs_texture_converter.checkBox_auto.setStyleSheet('''
                QCheckBox{
                color: rgb(255, 255, 255); 
                background-color: rgb(0, 140, 0);
                border: 1px solid rgb(0, 255, 0);
                }''')
            if self.ns_Version_rs_texture_converter.checkBox_overwrite.isChecked():
                self.ns_Version_rs_texture_converter.lineEdit_custom_input.setText(" -noskip")
            else:
                self.ns_Version_rs_texture_converter.lineEdit_custom_input.setText("")
            self.convertMode = 2
        else:
            self.ns_Version_rs_texture_converter.checkBox_auto.setStyleSheet('''
                QCheckBox{
                color: rgb(255, 170, 0);
                background-color: rgb(0, 0, 0);
                border: 1px solid rgb(0, 0, 0);
                }''')


    def setBooleanColors_custom(self, checkState):
        if checkState:
            self.ns_Version_rs_texture_converter.checkBox_lin.setChecked(False)
            self.ns_Version_rs_texture_converter.checkBox_lin.setStyleSheet('''
                QCheckBox{
                color: rgb(255, 170, 0);
                background-color: rgb(0, 0, 0);
                border: 1px solid rgb(0, 0, 0);
                }''')
            self.ns_Version_rs_texture_converter.checkBox_srgb.setChecked(False)
            self.ns_Version_rs_texture_converter.checkBox_srgb.setStyleSheet('''
                QCheckBox{
                color: rgb(255, 170, 0);
                background-color: rgb(0, 0, 0);
                border: 1px solid rgb(0, 0, 0);
                }''')
            self.ns_Version_rs_texture_converter.checkBox_auto.setChecked(False)
            self.ns_Version_rs_texture_converter.checkBox_auto.setStyleSheet('''
                QCheckBox{
                color: rgb(255, 170, 0);
                background-color: rgb(0, 0, 0);
                border: 1px solid rgb(0, 0, 0);
                }''')
            self.ns_Version_rs_texture_converter.checkBox_custom.setStyleSheet('''
                QCheckBox{
                color: rgb(255, 255, 255); 
                background-color: rgb(0, 140, 0);
                border: 1px solid rgb(0, 255, 0);
                }''')
            self.ns_Version_rs_texture_converter.lineEdit_custom_input.setEnabled(True)
            self.convertMode = 3
        else:
            self.ns_Version_rs_texture_converter.lineEdit_custom_input.setEnabled(False)
            self.ns_Version_rs_texture_converter.checkBox_custom.setStyleSheet('''
                QCheckBox{
                color: rgb(255, 170, 0);
                background-color: rgb(0, 0, 0);
                border: 1px solid rgb(0, 0, 0);
                }''')


    def closeEvent(self, event):
        self.hideDialog()


    def hideDialog(self):
        self.destroy()
        self.close()


    def gatherObjects(self):
        ## Disconnect Signal ##
        self.disconnect(self.ns_Version_rs_texture_converter.tableWidget_relevantObjects, QtCore.SIGNAL("cellChanged(int, int)"), self.changeTableWidgetValue)

        ## Lists/Data ##
        Formats = [".exr", ".jpg", ".png", ".tiff", ".tga"] ## Filesequences; UDIMS ##
        objectTypes = ["ogl", "tex", "env_map", "RS_campro_dofBokehImage", "profile"]
        pathArray = []

        ## Clear Table ##
        self.ns_Version_rs_texture_converter.tableWidget_relevantObjects.setRowCount(0)
        try:
            rowCount = self.ns_Version_rs_texture_converter.tableWidget_relevantObjects.rowCount()
            for parm, path in hou.fileReferences():
                if parm:
                    ## Debug ##
                    #print(parm.node().name() + " :: " + parm.name() + " :: " + parm.eval() )
                    ##
                    for rsObject in objectTypes:
                        if parm.name().find(rsObject) != -1:
                            ## Icon Seperation per Type ##
                            iconItem = QtWidgets.QTableWidgetItem()
                            iconItem.setIcon(hou.qt.Icon(parm.node().type().icon()))

                            if parm.eval().find("op:") == -1:
                                ## TableItems ##
                                convertItem = QTableWidgetItem("")
                                rstexbinItem = QTableWidgetItem("")
                                objectItem = QTableWidgetItem("")
                                typeItem = QTableWidgetItem("")
                                pathItem = QTableWidgetItem("")
                                counterItem = QTableWidgetItem("")
                                chkBoxItem = QtWidgets.QTableWidgetItem()
                                chkBoxItem.setFlags(QtCore.Qt.ItemIsEnabled)

                                ## Flag double References ##
                                flag = False
                                for i, path in enumerate(pathArray):
                                    if parm.eval() == path:
                                        flag = True

                                if not flag:
                                    pathArray.append(parm.eval())
                                    self.ns_Version_rs_texture_converter.tableWidget_relevantObjects.insertRow(rowCount)

                                    ## Find multiple/seq Files/UDIMS ##
                                    namePartSplit = parm.eval().split(".")
                                    namePartSplit.pop()
                                    namePart = ""
                                    for i in namePartSplit :
                                        namePart = namePart + i + "."

                                    pathPartSplit = parm.eval().split("/")
                                    pathPartSplit.pop()
                                    pathPart= ""
                                    for i in pathPartSplit:
                                        pathPart = pathPart + i + "/"

                                    folderContent = [f for f in os.listdir(pathPart) if os.path.isfile(os.path.join(pathPart, f))]

                                    counter = 0
                                    foundFiles = []
                                    for e, i in enumerate(folderContent):
                                        name = namePart.split("/")
                                        firstNamePart = name[-1].split(".")
                                        if i.find(firstNamePart[0]) != -1 and i.find(".rstexbin") == -1:
                                            counter = counter + 1
                                            foundFiles.append(pathPart + folderContent[e])

                                    ## Find existing rstexbin and Item setting ##
                                    flagSeq = True
                                    for i in foundFiles:
                                        namePartSplit = i.split(".")
                                        namePartSplit.pop()
                                        namePart = ""

                                        for i in namePartSplit:
                                            namePart = namePart + i + "."

                                        if not os.path.exists(namePart + "rstexbin"):
                                            flagSeq = False


                                    if flagSeq:
                                        rstexbinItem.setText("EXIST")
                                        rstexbinItem.setBackgroundColor(QColor(80, 80, 80))
                                        rstexbinItem.setTextColor(QColor(0, 255, 0))
                                        chkBoxItem.setIcon(iconUnselected)
                                        chkBoxItem.setToolTip("Exclude")
                                    else:
                                        rstexbinItem.setText("MISSING")
                                        rstexbinItem.setBackgroundColor(QColor(80, 80, 80))
                                        rstexbinItem.setTextColor(QColor(255, 0, 0))
                                        chkBoxItem.setIcon(iconSelected)
                                        chkBoxItem.setToolTip("Include")
                                    if counter == 0:
                                        rstexbinItem.setText("MISSING")
                                        rstexbinItem.setBackgroundColor(QColor(80, 80, 80))
                                        rstexbinItem.setTextColor(QColor(255, 0, 0))
                                        chkBoxItem.setIcon(iconUnselected)
                                        chkBoxItem.setToolTip("Exclude")


                                    rstexbinItem.setToolTip(parm.node().path())

                                    self.ns_Version_rs_texture_converter.tableWidget_relevantObjects.setItem(rowCount, 0, iconItem)
                                    self.ns_Version_rs_texture_converter.tableWidget_relevantObjects.setItem(rowCount, 1, rstexbinItem)

                                    objectItem.setText(parm.node().name())
                                    objectItem.setToolTip(parm.node().path())
                                    self.ns_Version_rs_texture_converter.tableWidget_relevantObjects.setItem(rowCount, 2, objectItem)

                                    typeItem.setText(parm.name())
                                    typeItem.setToolTip(parm.path())
                                    self.ns_Version_rs_texture_converter.tableWidget_relevantObjects.setItem(rowCount, 3, typeItem)

                                    if counter == 0:
                                        pathItem.setBackgroundColor(QColor(140, 0, 0))

                                    pathItem.setText(parm.unexpandedString())
                                    pathItem.setToolTip(parm.eval())
                                    self.ns_Version_rs_texture_converter.tableWidget_relevantObjects.setItem(rowCount, 4, pathItem)

                                    ## Counter setting ##
                                    counterItem.setText(str(counter))
                                    toolTipList = ""
                                    for i in foundFiles:
                                        toolTipList = toolTipList + i + "\n"
                                    toolTipList = toolTipList[:-1]
                                    counterItem.setToolTip(toolTipList)

                                    self.ns_Version_rs_texture_converter.tableWidget_relevantObjects.setItem(rowCount, 5, counterItem)
                                    self.ns_Version_rs_texture_converter.tableWidget_relevantObjects.setItem(rowCount, 6, chkBoxItem)

                                    ## Column sizing ##
                                    self.ns_Version_rs_texture_converter.tableWidget_relevantObjects.setColumnWidth(0, 50)
                                    self.ns_Version_rs_texture_converter.tableWidget_relevantObjects.setColumnWidth(1, 60)
                                    self.ns_Version_rs_texture_converter.tableWidget_relevantObjects.setColumnWidth(2, 100)
                                    self.ns_Version_rs_texture_converter.tableWidget_relevantObjects.setColumnWidth(3, 100)
                                    self.ns_Version_rs_texture_converter.tableWidget_relevantObjects.setColumnWidth(4, 600)
                                    self.ns_Version_rs_texture_converter.tableWidget_relevantObjects.setColumnWidth(5, 50)
                                    self.ns_Version_rs_texture_converter.tableWidget_relevantObjects.setColumnWidth(6, 50)
                                    rowCount += 1

            ## Re-adding Signal ##
            self.connect(self.ns_Version_rs_texture_converter.tableWidget_relevantObjects, QtCore.SIGNAL("cellChanged(int, int)"), self.changeTableWidgetValue)
            self.changeTableWidgetValue(0, 0)
        except Exception as e:
            print('Error on line {}'.format(sys.exc_info()[-1].tb_lineno), type(e).__name__, e)


try:
    dialog = ns_Version_rs_texture_converter()
except Exception as e:
    # print('Error on line {}'.format(sys.exc_info()[-1].tb_lineno), type(e).__name__, e)
    hou.ui.displayMessage("Something went wrong.")