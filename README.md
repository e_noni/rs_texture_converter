**RS Texture Converter 1.04**

* change to a scaleable gui


![Inline image](RS_Texture_Converter_1.03.jpg)


**RS Texture Converter 1.03**

* runs now as parent under the hou.qt.mainWindow() 
* usage of the native Houdini icons/*.svgs


**RS Texture Converter 1.02**

* gui change to grey theme
* exclude "op:" referenced texture sampler
* small fixes
* add resource .ui file


**RS Texture Converter 1.01**

* Add custom command field

-l Force linear gamma (recommended for floating point textures)
-s Force sRGB gamma (recommended for integer textures)
-wx -wy Filter MIP levels with wrapped filtering
-p Photometric IES data (for IES profile types)
-isphere Image Based Light (sphere projection)
-ihemisphere Image Based Light (hemisphere projection)
-imirrorball Image Based Light (mirrorball projection)
-iangularmap Image Based Light (angular map projection)
-ocolor Sprite Cut-Out Map Opacity (from color intensity)
-oalpha Sprite Cut-Out Map Opacity (from alpha)
-noskip Disable the skipping of textures determined to have already been converted


**RS Texture Converter 1.0**

* Converts referenced texture files to .rstexbins, inside SideFX Houdini.
(It also does a check if referenced texture files physically exist.)
* Supports file sequences.
* Supports UDIM files.

**Install:**
Copy the "script" folder and the "MainMenuCommon.xml" to your user folder.
e.g. "C:\Users\YourUser\Documents\houdini17.5"